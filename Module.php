<?php
namespace CSActionLayouts;
use Zend\Mvc\MvcEvent;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $sem = $e->getApplication()->getEventManager()->getSharedManager();

        $sem->attach( 'Zend\Mvc\Controller\AbstractController', 'dispatch', function($e)
        {
            $config = $e->getApplication()->getServiceManager()->get('config');
            $routeMatch = $e->getRouteMatch();
            $namespace = array_shift(explode('\\', $routeMatch->getParam('controller')));
            $controller = $e->getTarget();
            $controllerName = array_pop(explode('\\', $routeMatch->getParam('controller')));
            $actionName = strtolower($routeMatch->getParam('action'));

            // Use the layout assigned to the action
            if(isset($config['layouts'][$namespace]['controllers'][$controllerName]['actions'][$actionName]))
            {
                $controller->layout($config['layouts'][$namespace]['controllers'][$controllerName]['actions'][$actionName]);
            }
            // Use the controller default layout
            elseif(isset($config['layouts'][$namespace]['controllers'][$controllerName]['default']))
            {
                $controller->layout($config['layouts'][$namespace]['controllers'][$controllerName]['default']);
            }
            // Use the module default layout
            elseif(isset($config['layouts'][$namespace]['default']))
            {
                $controller->layout($config['layouts'][$namespace]['default']);
            }

        }, 10);
    }
}