#CSActionLayouts
Version 1.0 Created by David Bainbridge

##Summary
CSActionLayouts is an extended version of Evan Coury's EdpModuleLayouts module to allow
module, controller and action layouts to be defined in module config files with defaults at each level.

##Usage

1. Add the CSActionLayouts folder to your module directory in your application.
2. Add 'CSActionLayouts' to the list of modules to load in your /config/application.config.php file.
3. Add a 'layouts' section to any of your module config files (module.config.php) to configure which layouts are used.

Also make sure you register any new layouts in the module config file, e.g:

    return array(
        'view_manager' => array(
            'template_map' => array(
                'layout/default'    => __DIR__ . '/../view/layout/default.phtml',
                'layout/login'      => __DIR__ . '/../view/layout/login.phtml',
                'layout/admin'      => __DIR__ . '/../view/layout/admin.phtml'
            ),
        ),//...
    );

###Example config

As an example you might have a 'User' module for managing admins that can login.
The module has an admin controller and you want:

   - a 'login' action that uses a 'login' layout
   - a default 'admin' layout for when users are logged in and are using the admin controller
   - a default module layout titled 'default' for users not logged in (or using any other controller)

Example module.config.php

    return array(
        'layouts' => array(
            'User' => array(
                'controllers' => array(
                    'Admin' => array(
                        'actions' => array(
                            'login' => 'layout/login'
                        ),
                        'default' => 'layout/admin'
                    )
                ),
                'default' => 'layout/default'
            )
        )
    );